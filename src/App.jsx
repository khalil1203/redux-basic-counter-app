import { useSelector, useDispatch } from "react-redux";
import { counteraction } from "./store/CounterSlice";
import { Button, Card, Container } from "@mui/material";

function App() {
  const dispatch = useDispatch();
  const count = useSelector((e) => e.count.counter);
  const arr=useSelector((state)=>state.count.arr);
  const show=useSelector((state)=>state.count.show);
  console.log(arr);

  return (
    <Container >
      <Card sx={{display:"flex" ,flexDirection:"column", alignItems:"center", bgcolor:"green"}}>
      <h1 sx={{bgcolor:"white"}}>Basic Counter App Using Redux Toolkit</h1>

      <h2>{count}</h2>
      
      <Button sx={{color:"white"}} onClick={() => dispatch(counteraction.increment(count))}>increase</Button>

     {<Button sx={{color:"white"}} onClick={() => dispatch(counteraction.decrement())}>decrease</Button>}

      </Card>
    </Container>
  );
}

export default App;
