import { createSlice } from "@reduxjs/toolkit";


const counterslice=createSlice({

    name:"khalil",
    initialState:{
        counter:0,
        arr:[],
        show:false,
    },
    reducers:{
        increment(state,action){
            state.counter++;
            state.arr=[...state.arr,action.payload]
            state.show=!state.show
        },
        decrement(state,action){
            if(state.counter>0)
            state.counter--;
        }

    }
})
export const counteraction=counterslice.actions;

export default counterslice.reducer;