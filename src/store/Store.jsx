import { configureStore } from "@reduxjs/toolkit";
import counterslice from "./CounterSlice";


const store=configureStore({

    reducer:{
        count:counterslice,
        
    }

})
export default store;